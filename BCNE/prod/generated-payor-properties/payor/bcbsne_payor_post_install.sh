#!/bin/bash

CURR_DIR=$(cd "$(dirname "$0")"; pwd)
PROPERTY_FILE=$CURR_DIR"/bcbsne_payor_env.properties"

function getProperty {
   PROP_KEY=$1
   PROP_VALUE=`
   cat $PROPERTY_FILE | grep "$PROP_KEY" | cut -d'=' -f2`
   echo $PROP_VALUE
}

connectorProtocol=$(getProperty connectorProtocol) 
connectorHost=$(getProperty connectorHost) 
connectorPort=$(getProperty connectorPort) 
connectorHBRestServiceUser=$(getProperty connectorHBRestServiceUser) 
connectorHBRestServicePass=$(getProperty connectorHBRestServicePass) 
payorUser=$(getProperty payorUser)
payorHosts=$(getProperty payorHosts)
weblogicJarsDir=$(getProperty weblogicDomainDir)/jars
weblogicMappingFile=$(getProperty weblogicDomainDir)/data/defaultpathnamemapping.txt
AdminHost=$(getProperty AdminHost)
export IFS=","
for payorHost in $payorHosts; do
ssh -q  ${payorUser}@${payorHost} "sed -i -e 's|#CONNECTOR_PROTOCOL#|${connectorProtocol}|g' ${weblogicJarsDir}/HBIDPolicyConfig.properties"

ssh -q  ${payorUser}@${payorHost} "sed -i -e 's|#CONNECTOR_HOST#|${connectorHost}|g' ${weblogicJarsDir}/HBIDPolicyConfig.properties"

ssh -q  ${payorUser}@${payorHost} "sed -i -e 's|#CONNECTOR_PORT#|${connectorPort}|g' ${weblogicJarsDir}/HBIDPolicyConfig.properties"

ssh -q  ${payorUser}@${payorHost} "sed -i -e 's|#CONNECTOR_HB_REST_USER#|${connectorHBRestServiceUser}|g' ${weblogicJarsDir}/HBIDPolicyConfig.properties"

ssh -q  ${payorUser}@${payorHost} "sed -i -e 's|#CONNECTOR_HB_REST_PASSWORD#|${connectorHBRestServicePass}|g' ${weblogicJarsDir}/HBIDPolicyConfig.properties"


echo "Replacing defaultpathnamemapping on $payorHost"
ssh -q  ${payorUser}@${payorHost} "echo -n >  ${weblogicMappingFile}"
ssh -q  ${payorUser}@${payorHost} "cat > ${weblogicMappingFile} << EOF
COBPolicy=COB {0}
COBPolicy.policyType=COBPolicyType
COBPolicy.benefitPlanType=COBPlanType
COBPolicy.dateRanges.responsibilitySequenceCode=COBPriority
COBPolicy.dateRanges.startDate=COBStartDate
COBPolicy.dateRanges.endDate=COBEndDate
EOF"

done
echo "Updating muxer class for all claimservers"
scp -pq /home/ansible/hebb/dvatturi/scripts/muxer_update.py /home/ansible/hebb/dvatturi/scripts/update_muxer.sh ${payorUser}@${AdminHost}:~/
ssh ${payorUser}@${AdminHost} "cd ~/;  ~/update_muxer.sh"

exit 0

