#!/bin/bash

PROPERTY_FILE="/home/ansible/hebb/environments/MedCost/prod/generated-payor-properties/payor/medcost_payor_env.properties"

function getProperty {
   PROP_KEY=$1
   PROP_VALUE=`
   cat $PROPERTY_FILE | grep "$PROP_KEY" | cut -d'=' -f2`
   echo $PROP_VALUE
}

payorUser=$(getProperty payorUser)
payorHosts=$(getProperty payorHosts)
weblogicMappingFile=$(getProperty weblogicDomainDir)/data/defaultpathnamemapping.txt

export IFS=","
for payorHost in $payorHosts; do


echo "Replacing defaultpathnamemapping on $payorHost"
ssh -q  ${payorUser}@${payorHost} "echo -n >  ${weblogicMappingFile}"
ssh -q  ${payorUser}@${payorHost} "cat > ${weblogicMappingFile} << EOF
COBPolicy=COB {0}
COBPolicy.caseStatus=COB Status {0}
Membership.individual.primaryName.middleName=MiddleName
Membership.individual.primaryName.firstName=FirstName
Membership.individual.primaryName.lastName=LastName
MemberSelections.planSelections.enrolledPlan=BenefitPlan
Subscription.identifiedAccount=IdentifiedAccount
MemberSelections.planSelections.dateRanges.startDate=PlanStartDate
MemberSelections.planSelections.dateRanges.endDate=PlanEndDate
Membership.hccIdentifier=HccIdentifier
Membership.membershipUDTList.udtListValueSet.attrValueAsString=MemNationalSubNetworkUDTAttrVal
BenefitPlan.networkDefinition.networkDefinitionReference.networkDefinition=NetworkDefinitionCompID
MemberSelections.planSelections.dateRanges.benefitNetworks=MemberPlanBNFTNetwork
EOF"

done

exit 0

