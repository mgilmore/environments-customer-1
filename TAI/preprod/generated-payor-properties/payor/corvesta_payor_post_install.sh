#!/bin/bash

protocol=http 
host=100.102.132.11
payorUser=weblogic
payorHosts=172.19.37.161,172.19.37.162,172.19.37.163,172.19.37.164,172.19.37.165,172.19.37.167
accountTypes=ACT001,ACT002,ACT003,ACT004,ACT005,ACT006,ACT007, ACT104, ACT106
umdCodeForDFI=UMD254
attachmentType=ATC003
otherIdCodeEntry=IDT002

export IFS=","
for payorHost in $payorHosts; do

weblogicJarsDir=$(ssh -q  ${payorUser}@${payorHost} "echo \$BEA_HOME/user_projects/domains/\$WL_DOMAIN")
weblogicJarsDir=${weblogicJarsDir}'/jars'

ssh -q  ${payorUser}@${payorHost} "sed -i -e 's|#PROTOCOL#|${protocol}|g' ${weblogicJarsDir}/policy.properties"

ssh -q  ${payorUser}@${payorHost} "sed -i -e 's|#HOST#|${host}|g' ${weblogicJarsDir}/policy.properties"

ssh -q  ${payorUser}@${payorHost} "sed -i -e 's|#UMD_CODE_FOR_DFI#|${umdCodeForDFI}|g' ${weblogicJarsDir}/policy.properties"

ssh -q  ${payorUser}@${payorHost} "sed -i -e 's|#ATTACHMENT_TYPE#|${attachmentType}|g' ${weblogicJarsDir}/policy.properties"

ssh -q  ${payorUser}@${payorHost} "sed -i -e 's|#OTHER_ID_CODE_ENTRY#|${otherIdCodeEntry}|g' ${weblogicJarsDir}/policy.properties"

# Below properties are environment specific, please do not copy.

ssh -q  ${payorUser}@${payorHost} "sed -i -e 's|#ACCOUNT_TYPES#|${accountTypes}|g' ${weblogicJarsDir}/custom_loader.properties"

echo "Replaced custom property values in files on $payorHost"


done

exit 0

