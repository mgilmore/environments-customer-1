/*
 * This script contains sample data for the InsurancePurchaserGroupMetaData and ProductCategoryMetaData tables.
 *
 * Both of these tables must contain a single row (and never more) in order that the application will run correctly.
 *
 * The purpose of the InsurancePurchaserGroupMetaData is to describe the hierarchy of the Insurance Purchaser Group
 * (also known as Purchasing Entity) used at the Payer. The purpose of the ProductCategoryMetaData is to
 * describe the hierarchy of Product Categories used at the Payer. 
 *
 * The data in the single row inserted into each of these tables define the number of levels (maximum 6) and the labels
 * for each level. Once these are defined and the application begins to be used, these definitions should
 * never be subsequently changed, as they describe the structure of data within the application.
 *
 * This file is provided as a sample only and is not loaded into the schema by the scripts which create the initial
 * database. It is expected that a customer DBA will modify this script and run it after creating the initial schema.
 *
 * Author: Patrick Stephens
 * Company: EPAM Systems 1
 * Date: 10 August 2004
 *
*/

insert into T_PRODCAT_METADAT (
 PCMD_OBJ_STT,      -- Object State, must be set to '1'
 PCMD_LAS_ACT,      -- Last Action, must be set to '1'
 PCMD_LAS_ACT_TIM,  -- Last Action Time, should be set to 'sysdate'
 PCMD_CRTD,         -- Created Date, should be set to 'sysdate'
 PCMD_C4C_ID,       -- Click4Care (internal system) ID, should be set to 'PRODCATMETADATA'
 PCMD_ID,		  -- Primary/Unique key for this row, should be set to '1'
 CLASSTYPE,         -- ClassType, must be set to 'ProductCategoryMetaData'
 PCMD_NMBROF_LVLS,  -- Number of Product Category levels, should be configured by customer
 PCMD_LVL1LBL,      -- Label for Level 1, should be configured by customer
 PCMD_LVL2LBL,      -- Label for Level 2, should be configured by customer
 PCMD_LVL3LBL,      -- Label for Level 3, should be configured by customer
 PCMD_LVL4LBL,      -- Label for Level 4, should be configured by customer
 PCMD_LVL5LBL,      -- Label for Level 5, should be configured by customer
 PCMD_LVL6LBL       -- Label for Level 6, should be configured by customer
)
values (
 1,
 1,
 sysdate,
 sysdate,
 'PRODCATMETADATA',
 1,
 'ProductCategoryMetaData',
 6,                   -- edit this value
 'Company',  -- edit this value
 'SubCompany',          -- edit this value
 'Top Account ID',  -- edit this value
 'Line of Business',                -- edit this value
 'Product',                -- edit this value
 'Benefit Plan'                 -- edit this value
);


insert into T_INSCPRSRGRP_METADAT (
    IRGD_OBJ_STT,      -- Object State, must be set to '1'
    IRGD_LAS_ACT,      -- Last Action, must be set to '1'
    IRGD_LAS_ACT_TIM,  -- Last Action Time, should be set to 'sysdate'
    IRGD_CRTD,         -- Created Date, should be set to 'sysdate'
    IRGD_C4C_ID,       -- Click4Care (internal system) ID, should be set to 'IRGD_METADATA'
    IRGD_ID,	     -- Primary/Unique key for this row, should be set to '1'
    CLASSTYPE,         -- ClassType, must be set to 'InsurancePurchaserGroupMetaData'
    IRGD_NMBROF_LVLS,  -- Number of Purchasing Entity levels, should be configured by customer
    IRGD_LVL1LBL,      -- Label for Level 1, should be configured by customer
    IRGD_LVL2LBL,      -- Label for Level 2, should be configured by customer
    IRGD_LVL3LBL,      -- Label for Level 3, should be configured by customer
    IRGD_LVL4LBL,      -- Label for Level 4, should be configured by customer
    IRGD_LVL5LBL,      -- Label for Level 5, should be configured by customer
    IRGD_LVL6LBL       -- Label for Level 6, should be configured by customer
) values (
    1, 
    1,
    sysdate, 
    sysdate, 
    'IRGD_METADATA', 
    1,
    'InsurancePurchaserGroupMetaData',
    3,                -- edit this value
    'Account',        -- edit this value
    'SubAccount_1',       -- edit this value
    'SubAccount_2',          -- edit this value
    NULL,      -- edit this value
    NULL,             -- edit this value
    NULL              -- edit this value
);

commit;

exit

