#!/bin/bash

protocol=http 
host=100.102.132.11
payorUser=weblogic
payorHosts=172.29.4.153,172.29.4.154,172.29.4.155,172.29.4.156,172.29.4.157,172.29.4.158,172.29.4.164,172.29.4.225,172.29.4.226,172.29.4.173
#weblogicJarsDir=/home/weblogic/oracle/wls_12.2.1.4.0/user_projects/domains/preprod/jars
accountTypes=ACT001,ACT002,ACT003,ACT004,ACT005,ACT006,ACT007
umdCodeForDFI=UMD254
attachmentType=ATC003
otherIdCodeEntry=IDT002

export IFS=","
for payorHost in $payorHosts; do

weblogicJarsDir=$(ssh -q  ${payorUser}@${payorHost} "echo \$BEA_HOME/user_projects/domains/\$WL_DOMAIN")
weblogicJarsDir=${weblogicJarsDir}'/jars'

ssh -q  ${payorUser}@${payorHost} "sed -i -e 's|#PROTOCOL#|${protocol}|g' ${weblogicJarsDir}/policy.properties"

ssh -q  ${payorUser}@${payorHost} "sed -i -e 's|#HOST#|${host}|g' ${weblogicJarsDir}/policy.properties"

ssh -q  ${payorUser}@${payorHost} "sed -i -e 's|#UMD_CODE_FOR_DFI#|${umdCodeForDFI}|g' ${weblogicJarsDir}/policy.properties"

ssh -q  ${payorUser}@${payorHost} "sed -i -e 's|#ATTACHMENT_TYPE#|${attachmentType}|g' ${weblogicJarsDir}/policy.properties"

ssh -q  ${payorUser}@${payorHost} "sed -i -e 's|#OTHER_ID_CODE_ENTRY#|${otherIdCodeEntry}|g' ${weblogicJarsDir}/policy.properties"

# Below properties are environment specific, please do not copy.

ssh -q  ${payorUser}@${payorHost} "sed -i -e 's|#SUB_DOMAIN#|ddva|g' ${weblogicJarsDir}/corvesta_benefit_eligibility_service.properties"

ssh -q  ${payorUser}@${payorHost} "sed -i -e 's|#SECOND_LEVEL_DOMAIN#|prep|g' ${weblogicJarsDir}/corvesta_benefit_eligibility_service.properties"

ssh -q  ${payorUser}@${payorHost} "sed -i -e 's|#ACCOUNT_TYPES#|${accountTypes}|g' ${weblogicJarsDir}/custom_loader.properties"

echo "Replaced custom property values in files on $payorHost"


done

exit 0

