#!/bin/bash

protocol=http
host=100.102.196.6
payorUser=weblogic
payorHosts=172.29.4.24,172.29.4.25,172.29.4.26,172.29.4.27,172.29.4.28,172.29.4.33,172.29.4.34,172.29.4.35,172.29.4.29,172.29.4.36,172.29.4.47,172.29.4.48
#weblogicJarsDir=/home/weblogic/oracle/wls_12.2.1.4.0/user_projects/domains/prod/jars
accountTypes=ACT001,ACT002,ACT003,ACT004,ACT005,ACT006,ACT007
umdCodeForDFI=UMD254
attachmentType=ATC003
otherIdCodeEntry=IDT002

export IFS=","
for payorHost in $payorHosts; do

weblogicJarsDir=$(ssh -q  ${payorUser}@${payorHost} "echo \$BEA_HOME/user_projects/domains/\$WL_DOMAIN")
weblogicJarsDir=${weblogicJarsDir}'/jars'

ssh -q  ${payorUser}@${payorHost} "sed -i -e 's|#PROTOCOL#|${protocol}|g' ${weblogicJarsDir}/policy.properties"

ssh -q  ${payorUser}@${payorHost} "sed -i -e 's|#HOST#|${host}|g' ${weblogicJarsDir}/policy.properties"

ssh -q  ${payorUser}@${payorHost} "sed -i -e 's|#UMD_CODE_FOR_DFI#|${umdCodeForDFI}|g' ${weblogicJarsDir}/policy.properties"

ssh -q  ${payorUser}@${payorHost} "sed -i -e 's|#ATTACHMENT_TYPE#|${attachmentType}|g' ${weblogicJarsDir}/policy.properties"

ssh -q  ${payorUser}@${payorHost} "sed -i -e 's|#OTHER_ID_CODE_ENTRY#|${otherIdCodeEntry}|g' ${weblogicJarsDir}/policy.properties"

# Below properties are environment specific, please do not copy.

ssh -q  ${payorUser}@${payorHost} "sed -i -e 's|#SUB_DOMAIN#|ddva|g' ${weblogicJarsDir}/corvesta_benefit_eligibility_service.properties"

ssh -q  ${payorUser}@${payorHost} "sed -i -e 's|#SECOND_LEVEL_DOMAIN#||g' ${weblogicJarsDir}/corvesta_benefit_eligibility_service.properties"

ssh -q  ${payorUser}@${payorHost} "sed -i -e 's|#ACCOUNT_TYPES#|${accountTypes}|g' ${weblogicJarsDir}/custom_loader.properties"

echo "Replaced custom property values in files on $payorHost"

done

exit 0
