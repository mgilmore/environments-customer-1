#!/bin/bash


protocol=http 
host=100.102.162.47
payorUser=weblogic
payorHosts=172.29.4.208,172.29.4.210,172.29.4.211,172.29.4.212,172.29.4.213
#weblogicJarsDir=/home/weblogic/oracle/wls_12.2.1.4.0/user_projects/domains/staging/jars
accountTypes=ACT001,ACT002,ACT003,ACT004,ACT005,ACT006,ACT007
umdCodeForDFI=UMD254
attachmentType=ATC003
otherIdCodeEntry=IDT002

export IFS=","
for payorHost in $payorHosts; do

weblogicJarsDir=$(ssh -q  ${payorUser}@${payorHost} "echo \$BEA_HOME/user_projects/domains/\$WL_DOMAIN")
weblogicJarsDir=${weblogicJarsDir}'/jars'

ssh -q  ${payorUser}@${payorHost} "sed -i -e 's|#PROTOCOL#|${protocol}|g' ${weblogicJarsDir}/policy.properties"

ssh -q  ${payorUser}@${payorHost} "sed -i -e 's|#HOST#|${host}|g' ${weblogicJarsDir}/policy.properties"

ssh -q  ${payorUser}@${payorHost} "sed -i -e 's|#UMD_CODE_FOR_DFI#|${umdCodeForDFI}|g' ${weblogicJarsDir}/policy.properties"

ssh -q  ${payorUser}@${payorHost} "sed -i -e 's|#ATTACHMENT_TYPE#|${attachmentType}|g' ${weblogicJarsDir}/policy.properties"

ssh -q  ${payorUser}@${payorHost} "sed -i -e 's|#OTHER_ID_CODE_ENTRY#|${otherIdCodeEntry}|g' ${weblogicJarsDir}/policy.properties"
# Below properties are environment specific, please do not copy.

ssh -q  ${payorUser}@${payorHost} "sed -i -e 's|#SUB_DOMAIN#|ddva|g' ${weblogicJarsDir}/corvesta_benefit_eligibility_service.properties"

ssh -q  ${payorUser}@${payorHost} "sed -i -e 's|#SECOND_LEVEL_DOMAIN#|stage|g' ${weblogicJarsDir}/corvesta_benefit_eligibility_service.properties"

ssh -q  ${payorUser}@${payorHost} "sed -i -e 's|#ACCOUNT_TYPES#|${accountTypes}|g' ${weblogicJarsDir}/custom_loader.properties"

done

exit 0
