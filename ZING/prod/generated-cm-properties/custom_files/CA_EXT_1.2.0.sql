set serveroutput on
begin
  execute immediate 'drop index uix_ca_ipg';
exception when Others then null;
end;
/
-- Load additional configurable elements
create or replace procedure CA_SET_PROP(p_key VARCHAR2,p_val VARCHAR2)
as
begin
 insert into careadmin_props values (S_CAREADMIN_PROPS.nextval,p_key,p_val);
  exception when DUP_VAL_ON_INDEX then
    update careadmin_props set PROP_VAL = p_val where PROP_KEY=p_key;
end CA_SET_PROP;
/
begin
  CA_SET_PROP('COB_BENEFITPLAN_TYPE_ID','Medical');
  CA_SET_PROP('PHONETYPE_HOME_PHONE_ID','Home phone number');
  CA_SET_PROP('PHONETYPE_WORK_PHONE_ID','Work Phone Number');
  CA_SET_PROP('PHONETYPE_CONFIDENTIAL_PHONE_ID','');
  CA_SET_PROP('PHONETYPE_FAX_ID','Facsimile');
  CA_SET_PROP('PHONETYPE_MOBILE_ID','Mobile');
  CA_SET_PROP('CREATE_DUMMY_CPT','FALSE');
  CA_SET_PROP('CREATE_DUMMY_DX','FALSE');
  CA_SET_PROP('SUPPLIER_LOCATION_ENABLED','TRUE');
  CA_SET_PROP('PRACTITIONER_ROLE_ENABLED','TRUE');
  CA_SET_PROP('MEDICAID_ID_LABEL','Medicaid Number');
  CA_SET_PROP('OADD_2_ID', 'Deprecated');
  CA_SET_PROP('UDT_RRI_ID', '');
  CA_SET_PROP('UDT_RRI_VAL_ID', '');
  CA_SET_PROP('UDT_RRI_VAL', '');
  --UDT_MCD_ID, UDT_MCD_VALUE_ID need to be set for populating Medicaid Recertification Date from UDT
  --if Hicn medicaidRecertificationDate is not present
  CA_SET_PROP('UDT_MEDICAID_RECERTDT_ID', '');
  CA_SET_PROP('UDT_MEDICAID_RECERTDT_VALUE_ID', '');
  CA_SET_PROP('HOMEADD_ID', 'Residential');
  CA_SET_PROP('CONFIDENTIALADD_ID','');
  CA_SET_PROP('PREF_ADDRESS_ORDER','c,o2,b,o1,h');
  CA_SET_PROP('PREF_PHONE_ORDER','c,o2,o1,h,b');
  CA_SET_PROP('PREF_EMAIL_ORDER','c,e1,e2');  -- e1(business), e2(home)
  CA_SET_PROP('PROVIDER_PREF_ADDRESS_ORDER','b,o1');
  CA_SET_PROP('FAIL_ON_MISSING_BENP','FALSE');
  CA_SET_PROP('SUB_WITH_HISTORY_DELIMITER','<!--START_ADDITIONAL_INFORMATION-->');
  CA_SET_PROP('PRACTITIONER_MAPPING','DEFAULT'); -- DISABLED, LEGACY, DEFAULT

end;
/

-- de-couple benefit and account mapping these procedures are customizable per customer

create or replace procedure CA_PROCESS_BENEFIT(XD xmltype) as
    type A_LEVEL is VARRAY(4) of VARCHAR2(4000);
    ND xmltype;
    RET number := -1;
    R1 T_PRODCAT%ROWTYPE;
    IX number;
    v_id number;
    RVAL varchar2(4000);
    PATH varchar2(4000);
    LVL A_LEVEL := A_LEVEL();
    UNIVID varchar(4000);
    NAME_LEVEL A_LEVEL := A_LEVEL();
    BP T_BENSPKG%ROWTYPE;
    WC VARCHAR(4000);
begin
     R1 := C4C_DA.GET_PRODCAT_BY_UNIVID('CompanyName');

      -- get the LOB
      LVL.EXTEND(4);
      NAME_LEVEL.EXTEND(4);
      NAME_LEVEL(1) := R1.PDCY_NAM;
      NAME_LEVEL(2) := CA_LOAD.GETVAL(XD,'/node()/subCompanyName');
      NAME_LEVEL(3) := CA_LOAD.GETVAL(XD,'/node()/productName');
      NAME_LEVEL(4) := CA_LOAD.GETVAL(XD,'/node()/benefitPlanName');

      LVL(1) := R1.PDCY_NAM;
      LVL(2) := CA_LOAD.GETVAL(XD,'/node()/subCompanyName');
      LVL(3) := CA_LOAD.GETVAL(XD,'/node()/productName'); -- Product ID is not passing from Payor CA_LOAD.GETVAL(XD,'/node()/productId');
      LVL(4) := CA_LOAD.GETVAL(XD,'/node()/benefitPlanId');


      RVAL := CA_LOAD.GETVAL(XD,'/node()/benefitPlanId');
      V_ID := R1.PDCY_ID;

      for I in 2..4 LOOP
        UNIVID := LVL(I);

        R1 := C4C_DA.GET_PRODCAT_BY_UNIVID(UNIVID);
        R1.PDCY_NAM := NAME_LEVEL(I);
        R1.PDCY_PRNT := V_ID;
        R1.PDCY_UNIVID := UNIVID;

        if (I = 2) then
          R1.PDCY_C4C_ID := UNIVID;
        end if;
                R1.PDCY_LAS_ACT_TIM := sysdate;
        C4C_DA.SAVE_PRODCAT(R1);
        V_ID := R1.PDCY_ID;
      end LOOP;
      BP := C4C_DA.GET_BENEFITS_PACKAGE_BY_UNIVID(UNIVID);
      BP.BENP_NAM := NAME_LEVEL(4);
      BP.BENP_DSCN := UNIVID;
      BP.BENP_LWSTLVL_PRODCAT := V_ID;   -- This is done to associate to Lowest Level of ProdCat
      BP.BENP_LAS_ACT_TIM := sysdate;
          C4C_DA.SAVE_BENEFITS_PACKAGE(BP);
end CA_PROCESS_BENEFIT;
/

-- CAREMGR-33352
CREATE OR REPLACE PROCEDURE ca_get_prodcat_by_benplan(xd IN xmltype, pcat OUT T_PRODCAT%ROWTYPE) AS
BEGIN
  -- the benefiplan_id is typically in the lowest level univid in the T_PRODCAT.PDCY_UNIVID column.
  -- if this is not the case for a customer, then the query should be modified below
  SELECT tpc.*
  INTO pcat
  FROM T_PRODCAT tpc
  WHERE tpc.pdcy_id = (
    SELECT MAX(tpcsub.pdcy_id)
    FROM t_prodcat tpcsub
    WHERE tpcsub.PDCY_UNIVID LIKE('%' || CA_LOAD.GETVAL(XD, '/node()/benefitPlanId'))
    );
END ca_get_prodcat_by_benplan;
/

create or replace procedure CA_PROCESS_ACCOUNT(FRAG xmltype, PARENTID number) as
    NME VARCHAR2(4000);
    PID varchar2(4000);
    RET NUMBER;
    RV T_INSCPRSRGRP%ROWTYPE;
begin
      NME := replace(CA_LOAD.GETVAL(FRAG,'/node()/accountName'), '/', '-');
      PID := CA_LOAD.GETVAL(FRAG,'/node()/accountId');
      RV := CA_LOAD.get_ipg(PID,NME,PARENTID);
      RV.IPGP_NAM := NME;
      RV.IPGP_DSCN := NME;
      RV.IPGP_PRNT := PARENTID;
          RV.IPGP_LAS_ACT_TIM := sysdate;
      CA_LOAD.SAVE_IPG(RV);
      RET := RV.IPGP_ID;
      for c in (select * from TABLE(XMLSEQUENCE(CA_LOAD.GETNODE(FRAG,'/node()/childAccount')))) loop
        CA_PROCESS_ACCOUNT(c.column_value, RET);
      end LOOP;
end CA_PROCESS_ACCOUNT;
/
