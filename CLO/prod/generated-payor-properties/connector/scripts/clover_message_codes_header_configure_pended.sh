#!/bin/bash
set -e

HE_DIR=$1
HOST_NAME=$2

PROPERTY_FILE=./clover_env.properties

function getProperty {
   PROP_KEY=$1
   PROP_VALUE=`
   cat $PROPERTY_FILE | grep "$PROP_KEY" | cut -d'=' -f2`
   echo $PROP_VALUE
}

echo ""
echo "********** CLOVER SH : clover_pended_message_codes_header_extracts.sh start on ${HOST_NAME} **********"
    
echo "Start configuring eligibility job"

        jobConfigFile=$HE_DIR/clover-pendedClaimHeaderMessageExtracts/resources/claim-pended-header-message-codes-job-config.json
	jobCreationFile=$HE_DIR/clover-pendedClaimHeaderMessageExtracts/resources/claim-pended-header-message-codes-route-config.json
	
	echo $jobConfigFile
	echo $jobCreationFile
        
        chmod -R 755 $jobConfigFile
	chmod -R 755 $jobCreationFile

	sed -i -e "s|#HE_DIR#|${HE_DIR}|g" $jobConfigFile
	sed -i -e "s|#HE_DIR#|${HE_DIR}|g" $jobCreationFile

        extractStartEndTime=$(date +"%Y-%m-%dT%T.%3NEDT")
        echo "extractStartEndTime: ${extractStartEndTime}"

        sed -i -e "s|SSSS-MM-DDTHH:MM:SS.SSSZEDT|2017-10-17T20:57:25.944EDT|g" $jobConfigFile
        sed -i -e "s|EEEE-MM-DDTHH:MM:SS.SSSZEDT|2017-11-17T20:57:25.944EDT|g" $jobConfigFile
		

        extractOutputPath=$(getProperty message_codes_pended_header_extract_output_path)
        echo "extractOutputPath: ${extractOutputPath}"
	mkdir -p $extractOutputPath
        chmod -R 777 $extractOutputPath
	echo "extractOutputPath Created successfully"	

        sed -i -e "s|#EXTRACT_OUTPUT_PATH#|${extractOutputPath}|g" $jobConfigFile
	

	extractCron=$(getProperty message_codes_pended_header_extract_job_frequency)
        echo "extractCron: ${extractCron}"
	echo ""	
	sed -i -e "s|#EXTRACT_TIME#|${extractCron}|g" $jobCreationFile			
	jobCreationDirectory=$(grep "fileRequestBaseDir" $HE_DIR/etc/com.healthedge.he.common.scheduler.cfg | sed -e "s/fileRequestBaseDir=//g")
        jobCreationDirectoryfull=${jobCreationDirectory}/request/cron
        mkdir -p $jobCreationDirectoryfull
	chmod -R 777 $jobCreationDirectoryfull
	echo ""
	echo ""
	echo "jobCreationDirectory: ${jobCreationDirectoryfull}"
	echo "jobCreationDirectory Created successfully"
	echo Copying job creation file from $jobCreationFile to  $jobCreationDirectoryfull
	cp $jobCreationFile $jobCreationDirectoryfull

	jobConfigurationDirectory=$(grep "extractConfigRequestDir" $HE_DIR/etc/com.healthedge.he.common.extract.configuration.cfg | sed -e "s/extractConfigRequestDir=//g")
        echo ""
	echo ""
	echo "jobConfigurationDirectory: ${jobConfigurationDirectory}"
        mkdir -p $jobConfigurationDirectory
	chmod -R 777 $jobConfigurationDirectory	
        echo "Copying configuration json ${jobConfigFile} to ${jobConfigurationDirectory}"
        echo "jobConfigFile: "$jobConfigFile
	echo "jobConfigurationDirectory: "$jobConfigurationDirectory
	cp $jobConfigFile $jobConfigurationDirectory

    echo "End configuring eligibility pay job"


    echo "********** CLOVER SH : clover_pended_message_codes_header_extracts.sh.sh start on ${HOST_NAME} **********"
    echo ""

